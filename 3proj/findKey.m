function [  ] = findKey(  )
%SHOWGRAPH Summary of this function goes here
%   Detailed explanation goes here
numberOfTraces = 500;
traceSize = 470000;

offset = 0;
segmentLength = 470000;
                    
columns = 16;
rows = numberOfTraces;



traces = myload('kelloTraces.bin', traceSize, offset, segmentLength, numberOfTraces);
plaintext = myin('kelloplaintext.txt', columns, rows);
%ciphertext = myin('kellociphertext.txt', columns, rows);
%plaintext(:,1);
%ciphertext(:,1)'; % transoped
%linspace(1,255, 255); % jeden riadok
load('tab.mat')





byteStart = 1;
byteEnd = 16;
%keyCandidateStart = 0;
%keyCandidateStop = 255;



key = zeros(16,1);
% for every byte in the key do:
for BYTE=byteStart:byteEnd  
    
    % Create the power hypothesis matrix (dimensions: 
    % rows = numberOfTraces, columns = 256). 
    % The number 256 represents all possible bytes (e.g., 0x00..0xFF). 
%    powerHypothesis = zeros(numberOfTraces,256);
%    for K = keyCandidateStart:keyCandidateStop                            
        % --> create the power hypothesis here <--  
%    end;

v = zeros(numberOfTraces, 255);
for i=1:numberOfTraces
  for j=1:256
    v(i,j)=SubBytes(bitxor(plaintext(i,BYTE),j-1)+1);
  end
end
%v(1,1:20)
h = byte_Hamming_weight(v +1);
%h(1,1:20)


    % function mycorr returns the correlation coeficients matrix calculated
    % from the power consumption hypothesis matrix powerHypothesis and the
    % measured power traces. The resulting correlation coeficients stored in
    % the matrix CC are later used to extract the correct key.
%    CC = mycorr(powerHypothesis, traces);

CC = mycorr(h, traces(:,100000:170000));
    
    % --> do some operations here to find the correct byte of the key <--
[r,c] = find(CC==max(CC(:)));
key(BYTE) = r-1;

end;
dec2hex(key)
end