------------------------------------------------------------
-- classic_Adder
------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use work.classic_multiplier_parameters.all;

entity classic_Adder is
port (
  a, b: in std_logic_vector(M-1 downto 0);
  c: out std_logic_vector(M-1 downto 0)
);
end classic_Adder;

architecture simple of classic_Adder is

begin
	c <= a xor b;
end simple;