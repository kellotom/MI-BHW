------------------------------------------------------------
-- register
------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use work.classic_multiplier_parameters.all;

entity mul4 is
port (
  sel: in std_logic_vector(1 downto 0);
  a, b, c, d: in std_logic_vector(M-1 downto 0);
  y: out std_logic_vector(M-1 downto 0)
);
end mul4;

architecture simple of mul4 is
--  data std_logic_vector(M-1 downto 0);
begin
  y <= a when sel = "00" else
       b when sel = "01" else
       c when sel = "10" else
       d;
end simple;