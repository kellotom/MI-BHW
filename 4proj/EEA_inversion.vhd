


-----------------------------------
-- euclidean inversion (eea_inversion)
-----------------------------------
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;
use IEEE.std_logic_unsigned.all;
use work.eea_inversion_package.all;

entity eea_inversion is
port (
  A: in std_logic_vector (M-1 downto 0);
  clk, reset, start: in std_logic;
  Z: out std_logic_vector (M-1 downto 0);
  done: out std_logic
);
end eea_inversion;

architecture rtl of eea_inversion is

  COMPONENT eea_inversion_data_path
  PORT(
    r, s : IN std_logic_vector(M downto 0);
    u, v : IN std_logic_vector(M downto 0);
    d : IN std_logic_vector(logM downto 0);
    new_r, new_s : OUT std_logic_vector(M downto 0);
    new_u, new_v : OUT std_logic_vector(M downto 0);
    new_d : OUT std_logic_vector(logM downto 0)
    );
  END COMPONENT;

  signal count: natural range 0 to 2*M;
  type states is range 0 to 3;
  signal current_state: states;

  signal first_step, capture: std_logic;
  signal r, s, new_r, new_s : std_logic_vector(M downto 0);
  signal u, v, new_u, new_v: std_logic_vector(M downto 0);
  signal d, new_d: std_logic_vector(logM downto 0);
begin

  data_path_block: eea_inversion_data_path PORT MAP(
      r => r, s => s,
      u => u, v => v, d => d,
      new_r => new_r, new_s => new_s,
      new_u => new_u, new_v => new_v, new_d => new_d );

  z <= u(M-1 downto 0);

  process(clk, reset)
  begin
    if reset = '1' or first_step = '1' then
       r <= ('0' & A); s <= ('1' & Ireduc);
       u <= (0 => '1', others => '0'); v <= (others => '0');
       d <= (others => '0');
    elsif clk'event and clk = '1' then
      if capture = '1' then
        r <= new_r; s <= new_s;
        u <= new_u; v <= new_v;
        d <= new_d;
      end if;
    end if;
  end process;

  counter: process(reset, clk)
  begin
    if reset = '1' then count <= 0;
    elsif clk' event and clk = '1' then
      if first_step = '1' then
        count <= 0;
      elsif capture = '1' then
        count <= count+1;
    end if;
    end if;
  end process counter;

  control_unit: process(clk, reset, current_state, count)
  begin
    case current_state is
      when 0 to 1 => first_step <= '0'; done <= '1'; capture <= '0';
      when 2 => first_step <= '1'; done <= '0'; capture <= '0';
      when 3 => first_step <= '0'; done <= '0'; capture <= '1';
    end case;

    if reset = '1' then current_state <= 0;
    elsif clk'event and clk = '1' then
      case current_state is
      when 0 => if start = '0' then current_state <= 1; end if;
      when 1 => if start = '1' then current_state <= 2; end if;
      when 2 => current_state <= 3;
      when 3 => if count = 2*M-1 then current_state <= 0; end if;
      end case;
    end if;
  end process control_unit;

end rtl;
