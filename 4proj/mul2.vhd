------------------------------------------------------------
-- register
------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use work.classic_multiplier_parameters.all;

entity mul2 is
port (
  sel: in std_logic;
  a, b: in std_logic_vector(M-1 downto 0);
  y: out std_logic_vector(M-1 downto 0)
);
end mul2;

architecture simple of mul2 is
--  data std_logic_vector(M-1 downto 0);
begin
  y <= a when sel = '0' else
       b;
end simple;