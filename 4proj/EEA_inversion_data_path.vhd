----------------------------------------------------------------------------
-- Extended Euclidean Inversion (EEA_inversion.vhd)
--
-- Computes the 1/x mod f in GF(2**m)
-- Implements a sequential cincuit
--
----------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;
use IEEE.std_logic_unsigned.all;
package eea_inversion_package is

  constant M: integer := 79;
  constant logM: integer := 7;
  constant Ireduc: std_logic_vector(M-1 downto 0):= "000" & x"0000000000000000201"; --for M=128 bits
end eea_inversion_package;

-----------------------------------
-- eea_inversion data_path
-----------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use work.eea_inversion_package.all;

entity eea_inversion_data_path is
port (
  r, s: in std_logic_vector(M downto 0);
  u, v: in std_logic_vector(M downto 0);
  d: in STD_LOGIC_VECTOR (logM downto 0);
  new_r, new_s: out std_logic_vector(M downto 0);
  new_u, new_v: out std_logic_vector(M downto 0);
  new_d: out STD_LOGIC_VECTOR (logM downto 0)
);
end eea_inversion_data_path;

architecture rtl of eea_inversion_data_path is
  constant zero: std_logic_vector(logM downto 0):= (others => '0');

begin

  process(r,s,u,v,d)
  begin
    if R(m) = '0' then
      new_R <= R(M-1 downto 0) & '0';
      new_U <= U(M-1 downto 0) & '0';
      new_S <= S;
      new_V <= V;
      new_d <= d + 1;
    else
     if d = ZERO then
       if S(m) = '1' then
         new_R <= (S(M-1 downto 0) xor R(M-1 downto 0)) & '0';
         new_U <= (V(M-1 downto 0) xor U(M-1 downto 0)) & '0';
       else
         new_R <= S(M-1 downto 0) & '0';
         new_U <= V(M-1 downto 0) & '0';
       end if;
       new_S <= R;
       new_V <= U;
       new_d <= (0=> '1', others => '0');
     else --d /= ZERO
       new_R <= R;
       new_U <= '0' & U(M downto 1);
       if S(m) = '1' then
         new_S <= (S(M-1 downto 0) xor R(M-1 downto 0)) & '0';
         new_V <= (V xor U);
       else
         new_S <= S(M-1 downto 0) & '0';
         new_V <= V;
       end if;
       new_d <= d - 1;
     end if;
    end if;
  end process;

end rtl;
