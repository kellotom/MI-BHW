----------------------------------------------------------------------------
-- Classic Multiplier (classic_multiplier.vhd)
--
-- Computes the polynomial multiplication mod f in GF(2**m)
-- The hardware is genenerate for a specific f.
--
-- Defines 3 entities:
-- poly_multiplier: multiplies two m-bit polynomials and gives a 2*m-1 bits polynomial.
-- poly_reducer: reduces a (2*m-1)- bit polynomial by f to an m-bit polinomial
-- classic_multiplication: instantiates a poly_multiplier and a poly_reducer
-- and a Package (classic_multiplier_parameterse)
--
----------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;
use IEEE.std_logic_unsigned.all;
package classic_multiplier_parameters is
  constant M: integer := 79;
  constant Ireduc: std_logic_vector(M-1 downto 0):= "000" & x"0000000000000000201"; --for M=128 bits
  type matrix_reductionR is array (0 to M-1) of STD_LOGIC_VECTOR(M-2 downto 0);
  function reduction_matrix_R return matrix_reductionR;
end classic_multiplier_parameters;

package body classic_multiplier_parameters is
  function reduction_matrix_R return matrix_reductionR is
  variable R: matrix_reductionR;
  begin
  for j in 0 to M-1 loop
     for i in 0 to M-2 loop
        R(j)(i) := '0';
     end loop;
  end loop;
  for j in 0 to M-1 loop
     R(j)(0) := Ireduc(j);
  end loop;
  for i in 1 to M-2 loop
     for j in 0 to M-1 loop
        if j = 0 then
           R(j)(i) := R(M-1)(i-1) and R(j)(0);
        else
           R(j)(i) := R(j-1)(i-1) xor (R(M-1)(i-1) and R(j)(0));
        end if;
     end loop;
  end loop;
  return R;
end reduction_matrix_R;

end classic_multiplier_parameters;
