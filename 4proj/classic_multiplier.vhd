
------------------------------------------------------------
-- Classic Multiplication
------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use work.classic_multiplier_parameters.all;

entity classic_multiplication is
port (
  a, b: in std_logic_vector(M-1 downto 0);
  c: out std_logic_vector(M-1 downto 0)
);
end classic_multiplication;

architecture simple of classic_multiplication is
  component poly_multiplier port (
    a, b: in std_logic_vector(M-1 downto 0);
    d: out std_logic_vector(2*M-2 downto 0) );
  end component;
  component poly_reducer port (
    d: in std_logic_vector(2*M-2 downto 0);
    c: out std_logic_vector(M-1 downto 0));
  end component;

  signal d: std_logic_vector(2*M-2 downto 0);

begin

  inst_mult:  poly_multiplier port map(a => a, b => b, d => d);
  inst_reduc: poly_reducer port map(d => d, c => c);

end simple;