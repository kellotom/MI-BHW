
------------------------------------------------------------
-- poly_reducer
------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use work.classic_multiplier_parameters.all;

entity poly_reducer is
port (
  d: in std_logic_vector(2*M-2 downto 0);
  c: out std_logic_vector(M-1 downto 0)
);
end poly_reducer;

architecture simple of poly_reducer is
  constant R: matrix_reductionR := reduction_matrix_R;
  signal S: matrix_reductionR;
begin
  S <= R;
  gen_xors: for j in 0 to M-1 generate
    l1: process(d)
        variable aux: std_logic;
        begin
          aux := d(j);
          for i in 0 to M-2 loop
            aux := aux xor (d(M+i) and R(j)(i));
          end loop;
          c(j) <= aux;
    end process;
  end generate;

end simple;
