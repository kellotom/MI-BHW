----------------------------------------------------------------------------------
-- Company:
-- Engineer:
--
-- Create Date:    00:39:10 03/30/2017
-- Design Name:
-- Module Name:    BHW3top - Behavioral
-- Project Name:
-- Target Devices:
-- Tool versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;
use work.classic_multiplier_parameters.all;

entity BHW3top is
    Port ( start :  in STD_LOGIC;
           clk   :  in STD_LOGIC;
           reset :  in STD_LOGIC;
           done  : out STD_LOGIC;
           X0 : in   STD_LOGIC_VECTOR (M-1 downto 0);
           X1 : in   STD_LOGIC_VECTOR (M-1 downto 0);
           X2 : out  STD_LOGIC_VECTOR (M-1 downto 0);
           Y0 : in   STD_LOGIC_VECTOR (M-1 downto 0);
           Y1 : in   STD_LOGIC_VECTOR (M-1 downto 0);
           Y2 : out  STD_LOGIC_VECTOR (M-1 downto 0));
end BHW3top;

architecture Behavioral of BHW3top is

COMPONENT FSM is
	port (
LamSel, start, CLK, RESET, InvDone  : in std_logic;
      InvStart, M3, M4, M5, M6, EN_L, EN_D, EN_2, done : out std_logic;
      M1, M2: out std_logic_vector(1 downto 0)
			);
end COMPONENT;

COMPONENT mul2 is
port (
  sel: in std_logic;
  a, b: in std_logic_vector(M-1 downto 0);
  y: out std_logic_vector(M-1 downto 0)
);
end COMPONENT;

COMPONENT mul4 is
port (
  sel: in std_logic_vector(1 downto 0);
  a, b, c, d: in std_logic_vector(M-1 downto 0);
  y: out std_logic_vector(M-1 downto 0)
);
end COMPONENT;

COMPONENT myReg is
port (
  reset, en, clk: in std_logic;
  a: in std_logic_vector(M-1 downto 0);
  y: out std_logic_vector(M-1 downto 0)
);
end COMPONENT;

COMPONENT classic_Adder
port (
  a, b: in std_logic_vector(M-1 downto 0);
  c: out std_logic_vector(M-1 downto 0)
);
end COMPONENT;

COMPONENT classic_multiplication
port (
  a, b: in std_logic_vector(M-1 downto 0);
  c: out std_logic_vector(M-1 downto 0)
);
end COMPONENT;

COMPONENT eea_inversion
port (
  A: in std_logic_vector (M-1 downto 0);
  clk, reset, start: in std_logic;
  Z: out std_logic_vector (M-1 downto 0);
  done: out std_logic
);
end COMPONENT;

COMPONENT classic_squarer
port (
  en: in std_logic;
  a: in std_logic_vector(M-1 downto 0);
  c: out std_logic_vector(M-1 downto 0)
);
end COMPONENT;

  signal M1_SIG: std_logic_vector(M-1 downto 0);
  signal M2_SIG: std_logic_vector(M-1 downto 0);
  signal M3_SIG: std_logic_vector(M-1 downto 0);
  signal M4_SIG: std_logic_vector(M-1 downto 0);
  signal M5_SIG: std_logic_vector(M-1 downto 0);
  signal M6_SIG: std_logic_vector(M-1 downto 0);

  signal ADDER1_SIG: std_logic_vector(M-1 downto 0);
  signal ADDER2_SIG: std_logic_vector(M-1 downto 0);

  signal DOUBLE_SIG: std_logic_vector(M-1 downto 0);
  signal MUL_SIG: std_logic_vector(M-1 downto 0);
  signal INV_SIG: std_logic_vector(M-1 downto 0);

  signal LAM_SIG: std_logic_vector(M-1 downto 0);
  signal REG2_SIG: std_logic_vector(M-1 downto 0);

  signal EN_D_SIG: std_logic;
  signal EN_L_SIG: std_logic;
  signal EN_2_SIG: std_logic;

  signal M1SEL_SIG: std_logic_vector(1 downto 0);
  signal M2SEL_SIG: std_logic_vector(1 downto 0);
  signal M3SEL_SIG: std_logic;
  signal M4SEL_SIG: std_logic;
  signal M5SEL_SIG: std_logic;
  signal M6SEL_SIG: std_logic;

  signal LamSel_SIG: std_logic;

  signal startInv_SIG: std_logic;
  signal doneInv_SIG: std_logic;

  signal a: std_logic_vector(M-1 downto 0);
  signal inf: std_logic_vector(M-1 downto 0) := (others => '0');

begin
  a <= "100" & x"A2E38A8F66D7F4C385F";
  M1: mul4 port map(sel => M1sel_SIG, a => MUL_SIG, b => REG2_SIG, c => X0, d => Y0, y =>M1_SIG);
  M2: mul4 port map(sel => M2sel_SIG, a => Y1, b => X1, c => a, d => inf, y =>M2_SIG);
  Adder1: classic_Adder port map(a => M1_SIG, b => M2_SIG, c => ADDER1_SIG);
  M6: mul2 port map(sel => M6sel_SIG, a => MUL_SIG, b => ADDER1_SIG, y => M6_SIG);
  REGL: myReg port map( reset => reset, en => EN_L_SIG, clk => clk, a => M6_SIG, y => LAM_SIG);
  M3: mul2 port map(sel => M3sel_SIG, a => ADDER1_SIG, b => X1, y => M3_SIG);
  inver: eea_inversion PORT MAP(a => M3_SIG, z => INV_SIG, clk => clk, reset => reset, start => startInv_SIG, done => doneInv_SIG);
  M4: mul2 port map(sel => M4sel_SIG, a => ADDER1_SIG, b => INV_SIG, y => M4_SIG);
  M5: mul2 port map(sel => M5sel_SIG, a => LAM_SIG, b => Y1, y => M5_SIG);
  Multipl: classic_multiplication PORT MAP( a => M4_SIG, b => M5_SIG, c => MUL_SIG );
  Square: classic_squarer PORT MAP( a => LAM_SIG, c => DOUBLE_SIG, en => EN_D_SIG );
  Adder2: classic_Adder port map(a => DOUBLE_SIG, b => ADDER1_SIG, c => ADDER2_SIG);
  REG2: myReg port map( reset => reset, en => EN_2_SIG, clk => clk, a => ADDER2_SIG, y => REG2_SIG);

  myfsm: FSM port map( done => done, LamSel => LamSel_SIG, start => start, CLK => clk, RESET => reset, InvDone => doneInv_SIG,	InvStart => startInv_SIG, M3 => M3SEL_SIG, M4 => M4SEL_SIG, M5 => M5SEL_SIG, M6 => M6SEL_SIG, EN_L => EN_L_SIG, EN_D => EN_D_SIG, EN_2 => EN_2_SIG, M1 => M1SEL_SIG, M2 => M2SEL_SIG );

  LamSel_SIG <= '1' when (X0 /= X1) else '0';

  Y2 <= Y1     when (X0 = inf and Y0 = inf) else
        Y0     when (X1 = inf and Y1 = inf) else
        ADDER2_SIG when (LamSel_SIG = '1') else
        inf    when (Y0 /= Y1) else
        inf    when (X1 = inf) else
        ADDER2_SIG;
  X2 <= X1     when (X0 = inf and Y0 = inf) else
        X0     when (X1 = inf and Y1 = inf) else
        REG2_SIG when (LamSel_SIG = '1') else
        inf    when (Y0 /= Y1) else
        inf    when (X1 = inf) else
        REG2_SIG;

end Behavioral;

