-- Autor:     Tomas Kello
-- Mail:      kellotom@fit.cvut.cz
-- Datum:     4. 4. 2017
-- Predmet:   MI-BHW
-- Instituce: FIT CVUT Praha

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity FSM is
	port (	LamSel, start, CLK, RESET, InvDone	: in std_logic;
			InvStart, M3, M4, M5, M6, EN_L, EN_D , EN_2, done : out std_logic;
			M1, M2: out std_logic_vector(1 downto 0)
			);
end entity FSM;

architecture FSM_BODY of FSM is
	type TYP_STAV is (IDLE, decide, L11, L12, L13, L21, L22, S3, S4, S5, S6, S7 ); -- stavy automatu
	signal STAV, DALSI_STAV : TYP_STAV;
begin
	PRECHODY : process ( STAV, start, reset, InvDone )
	begin
		case STAV is
			when IDLE 		=>					-- prechody ze stavu IDLE
				if start = '1' then
					DALSI_STAV <= decide;
				else
					DALSI_STAV <= IDLE;
				end if;

			when decide 		=>
				if LamSel = '1' then
					DALSI_STAV <= L11;
				else
					DALSI_STAV <= L21;
				end if;

			when L11 		=>
				DALSI_STAV <= L12;

			when L12 		=>
				if InvDone = '1' then
					DALSI_STAV <= L13;
				else
					DALSI_STAV <= L12;
				end if;
			when L13 		=>
				DALSI_STAV <= S3;

			when L21 		=>
				if InvDone = '1' then
					DALSI_STAV <= L22;
				else
					DALSI_STAV <= L21;
				end if;
				
			when L22 		=>
				DALSI_STAV <= S3;

			when S3 		=>
				DALSI_STAV <= S4;

			when S4 		=>
				DALSI_STAV <= S5;

			when S5 		=>
				DALSI_STAV <= S6;

			when S6 		=>
				DALSI_STAV <= S7;
				
			when S7 		=>
				DALSI_STAV <= S7;

		end case;
	end process PRECHODY;


	VYSTUPY : process ( STAV )
	begin
		-- vynulovani vsech ridicich signalu
		EN_L <= '0';
		EN_D <= '0';
		EN_2 <= '0';
		M1 <= "00";
		M2 <= "00";
		M3 <= '0';
		M4 <= '0';
		M5 <= '0';
		M6 <= '0';
		done <= '0';
		InvStart <= '0';

		case STAV is
			when IDLE    =>

			when decide  =>

			when L11  =>
				M1 <= "11";
				M2 <= "00";
				M6 <= '1';
				EN_L <= '1';

			when L12  =>
				EN_L <= '0';
				M1 <= "10";
				M2 <= "01";
				M3 <= '0';
				M4 <= '1';
				M5 <= '0';
				M6 <= '0';
				InvStart <= '1';
				
			when L13  =>
				EN_L <= '1';
				M1 <= "10";
				M2 <= "01";
				M3 <= '0';
				M4 <= '1';
				M5 <= '0';
				M6 <= '0';

			when L21  =>
				EN_L <= '0';
				M1 <= "00";
				M2 <= "01";
				M3 <= '1';
				M4 <= '1';
				M5 <= '1';
				M6 <= '1';
				InvStart <= '1';
				
			when L22  =>
				EN_L <= '1';
				M1 <= "00";
				M2 <= "01";
				M3 <= '1';
				M4 <= '1';
				M5 <= '1';
				M6 <= '1';
				InvStart <= '1';

			when S3  =>
				EN_2 <= '1';
				M1 <= "10";
				M2 <= "01";

			when S4  =>
				M1 <= "01";
				M2 <= "10";
				EN_2 <= '1';
				EN_D <= '1';

			when S5  =>
				M1 <= "01";
				M2 <= "01";
				M4 <= '0';
				M5 <= '0';
				M6 <= '0';
				EN_L <= '1';

			when S6  =>
				M1 <= "01";
				M2 <= "00";
				EN_D <= '0';

			when S7  =>
				M1 <= "01";
				M2 <= "00";
				EN_D <= '0';
				done <= '1';

		end case;
	end process VYSTUPY;


	REG : process (CLK)
	begin
		if falling_edge(CLK) then
			if RESET = '1' then 			-- RESET AUTOMATU
				STAV <= IDLE;
		   else
           	STAV <= DALSI_STAV;
         end if;
      end if;
	end process REG;

end architecture FSM_BODY;
