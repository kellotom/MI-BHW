------------------------------------------------------------
-- register
------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use work.classic_multiplier_parameters.all;

entity myReg is
port (
  reset, en, clk: in std_logic;
  a: in std_logic_vector(M-1 downto 0);
  y: out std_logic_vector(M-1 downto 0)
);
end myReg;

architecture simple of myReg is
--  signal data: std_logic_vector(M-1 downto 0);
begin
 process( clk )
   begin
    if clk'event and clk='1'  then
      if reset = '1' then
        y <= (others => '0');
      elsif en = '1' then
        y <= a;
      end if;
    end if;
  end process;
--  y <= data;
end simple;