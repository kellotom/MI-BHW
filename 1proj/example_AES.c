// Ireducibilny polynom x8 + x4 + x3 + x + 1
unsigned char gmult(unsigned char a, unsigned char b) 
{
	unsigned char p = 0, i = 0, hbs = 0;
	for (i = 0; i < 8; i++)
	{
		if (b & 1)
		{
			p ^= a;
		}
		hbs = a & 0x80; // najvyssi clen ireduc. polynomu
		a <<= 1;
		if (hbs)
		{
			a ^= 0x1b; // zvysok polynomu
		}
		b >>= 1;
	}
	return p;
}


#define NumberOfColumns 4
#define NumberOfWords 4
#define NumberOfRounds 10

static unsigned char s_box[256] = {
	0x63, 0x7c, 0x77, 0x7b, 0xf2, 0x6b, 0x6f, 0xc5, 0x30, 0x01, 0x67, 0x2b, 0xfe, 0xd7, 0xab, 0x76,
	0xca, 0x82, 0xc9, 0x7d, 0xfa, 0x59, 0x47, 0xf0, 0xad, 0xd4, 0xa2, 0xaf, 0x9c, 0xa4, 0x72, 0xc0,
	0xb7, 0xfd, 0x93, 0x26, 0x36, 0x3f, 0xf7, 0xcc, 0x34, 0xa5, 0xe5, 0xf1, 0x71, 0xd8, 0x31, 0x15,
	0x04, 0xc7, 0x23, 0xc3, 0x18, 0x96, 0x05, 0x9a, 0x07, 0x12, 0x80, 0xe2, 0xeb, 0x27, 0xb2, 0x75,
	0x09, 0x83, 0x2c, 0x1a, 0x1b, 0x6e, 0x5a, 0xa0, 0x52, 0x3b, 0xd6, 0xb3, 0x29, 0xe3, 0x2f, 0x84,
	0x53, 0xd1, 0x00, 0xed, 0x20, 0xfc, 0xb1, 0x5b, 0x6a, 0xcb, 0xbe, 0x39, 0x4a, 0x4c, 0x58, 0xcf,
	0xd0, 0xef, 0xaa, 0xfb, 0x43, 0x4d, 0x33, 0x85, 0x45, 0xf9, 0x02, 0x7f, 0x50, 0x3c, 0x9f, 0xa8,
	0x51, 0xa3, 0x40, 0x8f, 0x92, 0x9d, 0x38, 0xf5, 0xbc, 0xb6, 0xda, 0x21, 0x10, 0xff, 0xf3, 0xd2,
	0xcd, 0x0c, 0x13, 0xec, 0x5f, 0x97, 0x44, 0x17, 0xc4, 0xa7, 0x7e, 0x3d, 0x64, 0x5d, 0x19, 0x73,
	0x60, 0x81, 0x4f, 0xdc, 0x22, 0x2a, 0x90, 0x88, 0x46, 0xee, 0xb8, 0x14, 0xde, 0x5e, 0x0b, 0xdb,
	0xe0, 0x32, 0x3a, 0x0a, 0x49, 0x06, 0x24, 0x5c, 0xc2, 0xd3, 0xac, 0x62, 0x91, 0x95, 0xe4, 0x79,
	0xe7, 0xc8, 0x37, 0x6d, 0x8d, 0xd5, 0x4e, 0xa9, 0x6c, 0x56, 0xf4, 0xea, 0x65, 0x7a, 0xae, 0x08,
	0xba, 0x78, 0x25, 0x2e, 0x1c, 0xa6, 0xb4, 0xc6, 0xe8, 0xdd, 0x74, 0x1f, 0x4b, 0xbd, 0x8b, 0x8a,
	0x70, 0x3e, 0xb5, 0x66, 0x48, 0x03, 0xf6, 0x0e, 0x61, 0x35, 0x57, 0xb9, 0x86, 0xc1, 0x1d, 0x9e,
	0xe1, 0xf8, 0x98, 0x11, 0x69, 0xd9, 0x8e, 0x94, 0x9b, 0x1e, 0x87, 0xe9, 0xce, 0x55, 0x28, 0xdf,
	0x8c, 0xa1, 0x89, 0x0d, 0xbf, 0xe6, 0x42, 0x68, 0x41, 0x99, 0x2d, 0x0f, 0xb0, 0x54, 0xbb, 0x16 };

void add_round_key(unsigned char *state, unsigned char *w, unsigned char r) {

	unsigned char c;

	for (c = 0; c < NumberOfColumns; c++) {
		state[NumberOfColumns * 0 + c] = state[NumberOfColumns * 0 + c] ^ w[4 * NumberOfColumns*r + 4 * c + 0];
		state[NumberOfColumns * 1 + c] = state[NumberOfColumns * 1 + c] ^ w[4 * NumberOfColumns*r + 4 * c + 1];
		state[NumberOfColumns * 2 + c] = state[NumberOfColumns * 2 + c] ^ w[4 * NumberOfColumns*r + 4 * c + 2];
		state[NumberOfColumns * 3 + c] = state[NumberOfColumns * 3 + c] ^ w[4 * NumberOfColumns*r + 4 * c + 3];
	}
}

void mix_columns(unsigned char *state) {

	unsigned char a[] = { 0x02, 0x01, 0x01, 0x03 }; // po stlpcoch sa posuva o jeden
	unsigned char i, j, col[4], res[4];

	for (j = 0; j < NumberOfColumns; j++) {
		for (i = 0; i < 4; i++) {
			col[i] = state[NumberOfColumns*i + j];
		}

			res[0] = gmult(a[0], col[0]) ^ gmult(a[3], col[1]) ^ gmult(a[2], col[2]) ^ gmult(a[1], col[3]);
			res[1] = gmult(a[1], col[0]) ^ gmult(a[0], col[1]) ^ gmult(a[3], col[2]) ^ gmult(a[2], col[3]);
			res[2] = gmult(a[2], col[0]) ^ gmult(a[1], col[1]) ^ gmult(a[0], col[2]) ^ gmult(a[3], col[3]);
			res[3] = gmult(a[3], col[0]) ^ gmult(a[2], col[1]) ^ gmult(a[1], col[2]) ^ gmult(a[0], col[3]);

		for (i = 0; i < 4; i++) {
			state[NumberOfColumns*i + j] = res[i];
		}
	}
}

void shift_rows(unsigned char *state)
{
	unsigned char i, k, s, tmp;
	for (i = 1; i < 4; i++) 
	{
		s = 0;
		while (s < i) {
			tmp = state[NumberOfColumns*i + 0];
			for (k = 1; k < NumberOfColumns; k++)
			{
				state[NumberOfColumns*i + k - 1] = state[NumberOfColumns*i + k];
			}
			state[NumberOfColumns*i + NumberOfColumns - 1] = tmp;
			s++;
		}
	}
}

void sub_bytes(unsigned char *state) {

	unsigned char i, j;
	unsigned char row, col;

	for (i = 0; i < 4; i++) {
		for (j = 0; j < NumberOfColumns; j++) {
			row = (state[NumberOfColumns*i + j] & 0xf0) >> 4;
			col = state[NumberOfColumns*i + j] & 0x0f;
			state[NumberOfColumns*i + j] = s_box[16 * row + col];
		}
	}
}

void sub_withSBox(unsigned char *w) // pre generovanie kluca
{
	unsigned char i;
	for (i = 0; i < 4; i++) 
	{
		w[i] = s_box[16 * ((w[i] & 0xf0) >> 4) + (w[i] & 0x0f)];
	}
}

void key_expansion(unsigned char *key, unsigned char *w)
{
	unsigned char tmp[4];
	unsigned char i;
	unsigned char len = NumberOfColumns*(NumberOfRounds + 1);

	for (i = 0; i < NumberOfWords*4; i++)
	{
		w[i] = key[i];
	}

	for (i = NumberOfWords; i < len; i++)
	{
		tmp[0] = w[4 * (i - 1) + 0];
		tmp[1] = w[4 * (i - 1) + 1];
		tmp[2] = w[4 * (i - 1) + 2];
		tmp[3] = w[4 * (i - 1) + 3];

		if (i%NumberOfWords == 0) 
		{
			// rotuj tmp
			unsigned char tmp2;
			tmp2 = tmp[0];
			for (unsigned char i = 0; i < 3; i++)
			{
				tmp[i] = tmp[i + 1];
			}
			tmp[3] = tmp2;

			sub_withSBox(tmp);

			unsigned char i2 = i / NumberOfWords;
			unsigned char Rcon;
			if (i2 == 1)
			{
				Rcon = 0x01;
			}
			else if (i2 > 1)
			{
				Rcon = 0x02;
				i2--;
				while (i2 - 1 > 0) 
				{
					Rcon = gmult(Rcon, 0x02);
					i2--;
				}
			}
			tmp[0] = tmp[0] ^ Rcon;
		}
		else if (NumberOfWords > 6 && i%NumberOfWords == 4)
		{
			sub_withSBox(tmp);
		}

		w[4 * i + 0] = w[4 * (i - NumberOfWords) + 0] ^ tmp[0];
		w[4 * i + 1] = w[4 * (i - NumberOfWords) + 1] ^ tmp[1];
		w[4 * i + 2] = w[4 * (i - NumberOfWords) + 2] ^ tmp[2];
		w[4 * i + 3] = w[4 * (i - NumberOfWords) + 3] ^ tmp[3];
	}
}

void transpose(unsigned char *inOut)
{
	//otoc maticu
	for (unsigned char i = 0; i < 4; i++)
	{
		for (unsigned char j = i + 1; j < NumberOfColumns; j++)
		{
			// swap hodnoty
			inOut[i + NumberOfColumns * j] = inOut[i + NumberOfColumns * j] + inOut[NumberOfColumns*i + j];
			inOut[NumberOfColumns * i + j] = inOut[i + NumberOfColumns * j] - inOut[NumberOfColumns*i + j];
			inOut[i + NumberOfColumns * j] = inOut[i + NumberOfColumns * j] - inOut[NumberOfColumns*i + j];
		}
	}
}

void cipher(unsigned char *inOut, unsigned char *w) 
{
	unsigned char r;

	transpose(inOut);

	add_round_key(inOut, w, 0);

	for (r = 1; r < NumberOfRounds; r++)
	{
		sub_bytes(inOut);
		shift_rows(inOut);
		mix_columns(inOut);
		add_round_key(inOut, w, r);
	}

	sub_bytes(inOut);
	shift_rows(inOut);
	add_round_key(inOut, w, NumberOfRounds);

	transpose(inOut);
}








/*
	This Project has been modified by Filip Stepanek <filip.stepanek@fit.cvut.cz>,
	FIT-CTU <www.fit.cvut.cz/en> for the purpose of smartcard education 
	using the SOSSE <http://www.mbsks.franken.de/sosse/html/index.html> 
	created by Matthias Bruestle and files 	from the Chair for Embedded Security (EMSEC), 
	Ruhr-University Bochum <http://www.emsec.rub.de/chair/home/>.
*/


#include "example_AES.h"
#include <avr/io.h>

/**
 *	set the trigger PIN
 */
#define set_pin(port, value) ((port)|=(value))
/**
 *	clear the trigger PIN
 */
#define clear_pin(port, value) ((port)&=(value))

/*...*/

unsigned char buffer[16];

void encrypt_aes_16(unsigned char *in, unsigned char *out, unsigned char *skey)
{

	//... Initialize ...
	
	// set trigger PIN
	set_pin(DDRB, 0b10100000);
	set_pin(PORTB, 0b10100000);

	unsigned char *expansedKey;

	static unsigned char e[176];
	expansedKey = e;

	key_expansion(skey, expansedKey);

	cipher(in , expansedKey );
	
	for (unsigned char i = 0; i < 16; i++)
	{
		out[i] = in[i];
	}
	



	// clear trigger PIN
	clear_pin(PORTB, 0b01011111);

	//... Copy output ...
}


