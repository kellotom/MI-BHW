function [  ] = showGraph(  )
numberOfTraces = 500;
traceSize = 470000;

offset = 0;
segmentLength = 470000;

traces = myload('kelloTraces.bin', traceSize, offset, segmentLength, numberOfTraces);

figure

subplot(3,1,1)       % add first plot in 2 x 1 grid
plot(traces(1,:))
title('1. vzorok')

subplot(3,1,2)       % add second plot in 2 x 1 grid
plot(traces(11,:))
title('11. vzorok')

subplot(3,1,3)       
plot(traces(500,:))
title('500. vzorok')
end


